package edu.sda.dayTwo.arrays.distanceExercise;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ArrayMaxDistTest {

    ArrayMaxDist classToTest = new ArrayMaxDist();

    @Test
    public void testMinAndMaxFromList() {
    //given
    List<Integer> input = Arrays.asList(1, 9, 109, 3);

    //when
    MinMaxModel actualResult = classToTest.listDist(input);

    //then
    assert (actualResult.getMax().equals(108));
    assert (actualResult.getMin().equals(2));
    }

    @Test(expected = NullPointerException.class)
    public void testMinAndMaxFromListNPE () {
        //todo add case that throws npe
    }

    //todo add test for array case as well

}

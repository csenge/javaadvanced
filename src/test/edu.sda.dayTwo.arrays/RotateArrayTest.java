package edu.sda.dayTwo.arrays;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RotateArrayTest {

    RotateArray classToTest = new RotateArray();

    @Test
    public void testRotation() {
        //given
        Integer[] array = {1, 2, 3};
        Integer[] expectedResult = {3,1,2};

        //when
        Integer[] actualResult = classToTest.rotate(array);

        //then
        assertEquals(expectedResult, actualResult);

    }

    @Test(expected = NullPointerException.class)
    public  void testRotationNullInput() {

        //when
        Integer[] actualResult = classToTest.rotate(null);

    }
}

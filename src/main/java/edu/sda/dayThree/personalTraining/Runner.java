package edu.sda.dayThree.personalTraining;

import edu.sda.dayThree.personalTraining.model.Exercise;
import edu.sda.dayThree.personalTraining.model.Gym;
import edu.sda.dayThree.personalTraining.model.Supplement;
import edu.sda.dayThree.personalTraining.model.Trainee;
import edu.sda.dayThree.personalTraining.service.RegistrationService;
import edu.sda.dayThree.personalTraining.service.SimulationService;

public class Runner {

    public static void main(String[] args) {
        //define services
        SimulationService simulationService = new SimulationService();

        //define params
        Gym worldclassPremium = initializeGym();

        System.out.println("Current status of the gym: " + worldclassPremium.toString());
        System.out.println("-----------------------------------------");

        //simulate training
        simulationService.simulateAnExercise(worldclassPremium.getGymCustomer().get(0), worldclassPremium.getExerciseList().get(0));

    //simulationService.simulateTakingSupplement(traineeCsenge, exercise1); --> get first customer, make it take the first suppliment for testing purposes
        simulationService.simulateTakingSupplement(worldclassPremium.getGymCustomer().get(0), worldclassPremium.getSupplements().get(0));


        simulationService.simulateAnExercise(worldclassPremium.getGymCustomer().get(0), worldclassPremium.getExerciseList().get(0));


        simulationService.strongestTraineeDoesHardestExercise(worldclassPremium);
        simulationService.doEasiestGroupExercise(worldclassPremium);

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        simulationService.makeMembersTired(worldclassPremium);
    }

    private static Gym initializeGym() {
        RegistrationService registrationService = new RegistrationService();
        Gym worldclassPremium = new Gym();
        Trainee traineeCsenge = registrationService.registerTrainee("Csenge", 5, 2, worldclassPremium);
        Trainee traineeAna = registrationService.registerTrainee("Ana", 35, 25, worldclassPremium);
        Trainee traineeIon = registrationService.registerTrainee("Ion", 50, 70, worldclassPremium);
        Trainee traineeElena = registrationService.registerTrainee("Elena", 15, 10, worldclassPremium);
        Trainee traineeLiviu= registrationService.registerTrainee("Liviu", 15, 10, worldclassPremium);

        //Trainee traineeSlab = registrationService.registerTrainee("Slab", 0, 1); todo handle this exception when it is the custom exception

        Exercise exercise1 = new Exercise(3, 3);
        registrationService.registerNewExercise(exercise1, worldclassPremium);
        Exercise exercise2 = new Exercise(7, 5);
        registrationService.registerNewExercise(exercise2, worldclassPremium);
        Exercise exercise3 = new Exercise(1, 1);
        registrationService.registerNewExercise(exercise3, worldclassPremium);
        Exercise exercise4 = new Exercise(2, 3);
        registrationService.registerNewExercise(exercise4, worldclassPremium);

        Supplement supplement1 = new Supplement(10);
        registrationService.registerNewSuppliment(supplement1, worldclassPremium);
        Supplement supplement2 = new Supplement(3);
        registrationService.registerNewSuppliment(supplement2, worldclassPremium);
        Supplement supplement3 = new Supplement(9);
        registrationService.registerNewSuppliment(supplement3, worldclassPremium);
        Supplement supplement4 = new Supplement(7);
        registrationService.registerNewSuppliment(supplement4, worldclassPremium);

        return worldclassPremium;
    }
}

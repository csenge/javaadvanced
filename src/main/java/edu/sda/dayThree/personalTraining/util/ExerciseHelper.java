package edu.sda.dayThree.personalTraining.util;

public class ExerciseHelper {

    public static boolean validatePositiveValue(int updatedStamina) {
        return updatedStamina > 0;
    }

    public static boolean validatePositiveNonNullValue(int value) {
        return value >= 0;
    }
}

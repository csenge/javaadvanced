package edu.sda.dayThree.personalTraining.service;

import edu.sda.dayThree.personalTraining.model.Exercise;
import edu.sda.dayThree.personalTraining.model.Gym;
import edu.sda.dayThree.personalTraining.model.Supplement;
import edu.sda.dayThree.personalTraining.model.Trainee;
import edu.sda.dayThree.personalTraining.util.ExerciseHelper;

public class RegistrationService {

    /**
     * This method creates a new trainee - TODO after jdbc module insert trainee in DB
     * @param name name of trainee
     * @param stamina stamina of trainee
     * @param strength strength of trainee
     * @param worldclassPremium
     * @return trainee registered
     */
    public Trainee registerTrainee(String name, int stamina, int strength, Gym worldclassPremium) {
        // stamina should be > 0. otherwise trainee should not be registered (return null) OR exception should be thrown
        if (!ExerciseHelper.validatePositiveNonNullValue(stamina)) {
//            return null;
            throw new RuntimeException("Stamina should not be below 0."); //todo create custom InvalidStaminaException
        }

        Trainee traineeToAdd = new Trainee(name, stamina, strength);

        worldclassPremium.getGymCustomer().add(traineeToAdd);
        return traineeToAdd;
    }

    public void registerNewExercise(Exercise exercise, Gym gym) {
        gym.getExerciseList().add(exercise);
    }

    public void registerNewSuppliment(Supplement supplement, Gym gym) {
        gym.getSupplements().add(supplement);
    }
}

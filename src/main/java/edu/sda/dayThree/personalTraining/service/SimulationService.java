package edu.sda.dayThree.personalTraining.service;

import edu.sda.dayThree.personalTraining.model.Exercise;
import edu.sda.dayThree.personalTraining.model.Gym;
import edu.sda.dayThree.personalTraining.model.Supplement;
import edu.sda.dayThree.personalTraining.model.Trainee;

import java.util.Comparator;
import java.util.stream.Collectors;

public class SimulationService {

    private ExerciseService exerciseService = new ExerciseService();

    /**
     * This method simulates an exercise and prints before after statuses
     * @param trainee person doing the exercise
     * @param exercise exercise to be executed
     */
    public void simulateAnExercise(Trainee trainee, Exercise exercise) {
        //todo replace souts with stringbuilder/stringbuffer
        System.out.println("Trainee before exercise: " + trainee);
        exerciseService.executeExercise(trainee, exercise);

        System.out.println("trainee after exercise" + trainee);
        System.out.println("-----------------------------------------");
    }

    /**
     * This method simulates taking a supplement and prints before and after values.
     * @param trainee the one taking the supplement
     * @param supplement desired supplement
     */
    public void simulateTakingSupplement(Trainee trainee, Supplement supplement) {
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append("Trainee before supplement: ");
        resultBuilder.append(trainee);
        resultBuilder.append("\n");


        exerciseService.takeSupplements(trainee, supplement);

        resultBuilder.append("Trainee after supplement: ");
        resultBuilder.append(trainee);
        resultBuilder.append("\n");
        resultBuilder.append("-----------------------------------------");
        resultBuilder.append("\n");
        System.out.println(resultBuilder.toString());

    }

    /**
     *  This method will find the strongest trainee (highest strength) and will make him execute the harders exercise from the registered exercise list.
     * @param gym - contains list of trainees and list of exercises
     */
    public void strongestTraineeDoesHardestExercise(Gym gym) {
        Trainee strongestPerson = gym.getGymCustomer().stream()
                .sorted(Comparator.comparing(Trainee::getStrength))
                //todo try and reverse this
                .collect(Collectors.toList())
                .get(gym.getGymCustomer().size() - 1);
        Exercise hardestExercise = gym.getExerciseList().stream()
                .sorted(Comparator.comparing(Exercise::getIncreaseStrength))

                .collect(Collectors.toList())
                .get(gym.getExerciseList().size() - 1);
        System.out.println("Strongest person in the gym: " + strongestPerson);
        System.out.println("Hardest exercise: " + hardestExercise);
        System.out.println("-----------------------------------------");
        simulateAnExercise(strongestPerson, hardestExercise);
    }


    /**
     * Each person should do a different exercises. No two trainees with the same exercise.
     * ex: t1 - e1, t2- e2, t3-e3
     * Make exercises random.
     * @param gym
     */
    public void individualExercises(Gym gym) {

    }

    /**
     * Select the person with highest stamina in the gym.
     * Select the person with lowest stamina in the gym.
     *
     * Make the first person do so many exercises and the second one so many supplements that they trade places.
     * @param gym
     */
    public void switchByStamina(Gym gym) {

    }

    /**
     * Make everyone tired. Each person in the gym should have stamina < 10
     */
    public void makeMembersTired(Gym gym) {
        Exercise exercise = gym.getExerciseList().get(1); // todo verify if it is better to have the hardest exercise


        /**
         * 1. iterate over trainees once. if trainee has stamina > 10 -> make him do an exercise. repeat until stamina < 10
         *
         * 2. while there are trainees with stamina > 10 -> filter trainees with high stamina -> make them do exercises
         */
        //version 1
        gym.getGymCustomer().stream()
                .filter(trainee -> trainee.getStamina() > 10)
                .forEach(trainee -> {
                    while (trainee.getStamina() > 10) {
                        simulateAnExercise(trainee, exercise);
                    }
                });

        //version 2
//        while (gym.getGymCustomer().stream().filter(trainee -> trainee.getStamina() > 10).findFirst().isPresent()) {
//           gym.getGymCustomer().stream()
//                   .filter(trainee -> trainee.getStamina() > 10)
//                   .forEach(trainee -> simulateAnExercise(trainee, exercise));
//       }
    }

    /**
     *  Make every gym member that has stamina > 15 do an exercise.
     */
    public void doExerciseIfStaminaTooHigh(Gym gym) {
        // same as below todo
    }

    /**
     * Give a random supplement for all customers that have low stamina (low means stamina below 10)
     */
    public void takeSupplementIfNeeded(Gym gym) { //todo test this from runner
        Supplement supplement = gym.getSupplements().get(2);

        gym.getGymCustomer().stream()
                .filter(trainee -> trainee.getStamina() < 10)
                .forEach(trainee -> simulateTakingSupplement(trainee, supplement));
    }

    /**
     * Select a random person from the gym. Select a random exercise. Make the person execute the found exercise.
     * @param gym
     */
    public void randomPersonRandomExercise(Gym gym) {
        int randomNumber = -1; //todo generate a random number
        Exercise randomExercise = null; //todo
        Trainee randomTrainee = gym.getGymCustomer().get(randomNumber);

    }

    /**
     * Select the easiest exercise and make all customers execute it.
     * @param gym
     */
    public void doEasiestGroupExercise(Gym gym) {
        Exercise easiestExercise = gym.getExerciseList().stream()
                    .sorted(Comparator.comparing(Exercise::getDecreaseStamina))
                    .collect(Collectors.toList())
                .get(0);

        System.out.println("Easiest exercise is: " + easiestExercise);
        //make group execute it
        gym.getGymCustomer().stream()
                .forEach(trainee -> simulateAnExercise(trainee, easiestExercise));
    }



    /**
     * Select randomly an exercise from the list. Make the whole group execute it. Verify if everyone is able to do it (no stamina should drop below 0)
     *
     * @param gym
     */
    //todo call this method from runner
    public void doRandomGroupExercise(Gym gym) {
        Exercise randomExercise = null; // todo

        //make whole group execute it
        System.out.println("Everyone is doing the same exercise!");

        //varianta 1 - for simplu
        for (Trainee trainee: gym.getGymCustomer()) {
            simulateAnExercise(trainee, randomExercise);
        }

        //varianta 2 - cu stream
        gym.getGymCustomer().stream()
                .forEach( trainee -> simulateAnExercise(trainee, randomExercise));
    }
}

package edu.sda.dayThree.personalTraining.service;

import edu.sda.dayThree.personalTraining.model.Exercise;
import edu.sda.dayThree.personalTraining.model.Supplement;
import edu.sda.dayThree.personalTraining.model.Trainee;
import edu.sda.dayThree.personalTraining.util.ExerciseHelper;

public class ExerciseService {

    /**
     * This method will update the trainee - increase strength and decrease stamina based on the exercise executed
     * @param trainee - person doing the exercise
     * @param exercise - desired exercise to be done
     * @return - updated trainee
     */
    public Trainee executeExercise(Trainee trainee, Exercise exercise) {
        if (ExerciseHelper.validatePositiveValue(trainee.getStamina() - exercise.getDecreaseStamina())) {
            trainee.setStamina(trainee.getStamina() - exercise.getDecreaseStamina());
        } else {
            throw new RuntimeException("Stamina should not get below 0"); //todo throw previously defined exception
        }
        trainee.setStrength(trainee.getStrength() + exercise.getIncreaseStrength());

        return trainee;
    }

    /**
     * This method will update the trainee and return it with the increased stamina based on the supplment taken
     * @param trainee - who takes the supplement
     * @param supplement - desired supplement to be taken
     * @return - updated trainee
     */
    public Trainee takeSupplements(Trainee trainee, Supplement supplement) {
        trainee.setStamina(trainee.getStamina() + supplement.getIncreaseStamina());

        return trainee;
    }
}

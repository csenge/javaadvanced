package edu.sda.dayThree.personalTraining.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Supplement {

    @Getter
    @Setter
    private int increaseStamina;

    @Override
    public String toString() {
        return "Supplement{" +
                "increaseStamina=" + increaseStamina +
                '}';
    }
}

package edu.sda.dayThree.personalTraining.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class Gym {

    @Getter
    @Setter
    private List<Trainee> gymCustomer = new ArrayList<>();

    @Setter
    @Getter
    private List<Supplement> supplements = new ArrayList<>();

    @Getter
    @Setter
    private List<Exercise> exerciseList = new ArrayList<>();

    @Override
    public String toString() {
        return "Gym{" +
                "gymCustomer=" + gymCustomer +
                ", supplements=" + supplements +
                ", exerciseList=" + exerciseList +
                '}';
    }
}

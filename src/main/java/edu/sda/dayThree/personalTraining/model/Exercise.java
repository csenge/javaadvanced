package edu.sda.dayThree.personalTraining.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Exercise {

    @Getter
    @Setter
    private int increaseStrength;

    @Getter
    @Setter
    private int decreaseStamina;

    @Override
    public String toString() {
        return "Exercise{" +
                "increaseStrength=" + increaseStrength +
                ", decreaseStamina=" + decreaseStamina +
                '}';
    }
}

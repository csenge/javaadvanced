package edu.sda.dayThree.personalTraining.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Comparator;

@NoArgsConstructor
@AllArgsConstructor
public class Trainee {

    //.sorted(Comparator.reverseOrder())
    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private int stamina;

    @Getter
    @Setter
    private int strength;

    @Override
    public String toString() {
        return "Trainee{" +
                "name='" + name + '\'' +
                ", stamina=" + stamina +
                ", strength=" + strength +
                '}';
    }

}


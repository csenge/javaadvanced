package edu.sda.dayThree.basicLibrary.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Library {

    @Getter
    @Setter
    List<Book> books = new ArrayList<>();
}

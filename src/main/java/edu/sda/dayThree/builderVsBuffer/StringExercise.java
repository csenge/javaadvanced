package edu.sda.dayThree.builderVsBuffer;

public class StringExercise {

    /**
     * Compares the behaviour of StringBuilder and StringBuffer. It was said that buffer is thread safe, but it was discovered that it is not.
     * In this sense, there is no more difference between the two classes.
     *
     * TO BE AWARE OF THIS AT INTERVIEWS.
     *
     * @param args
     */
    public static void main(String[] args) {
        // StringBuilder and StringBuffer are not immutable, hence can do the append without reinitializing stringBuilder and Stringbuffer variables
        //create stringBuilder with initial value
        StringBuilder stringBuilder = new StringBuilder("asd");
//        stringBuilder.append("asd");
        stringBuilder.append("test");

        // should be threadSafe - is not (s-a descoperit acum recent)
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("asd");
        stringBuffer.append("test");

        System.out.println("Builder response: " + stringBuilder.toString());
        System.out.println("Buffer response: " + stringBuffer.toString());

        //string IS immutable --> simple concatenation without reinit will not be changing the value
        String string = "";
         string = string.concat("asd");
        string.concat("test");

        System.out.println("String response: " + string);
    }
}

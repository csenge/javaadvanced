package edu.sda.dayThree.switchReplacer;

public class JavaSwitchReplacerMain {


    /**
     *
     * input: String
     * output: String
     *
     * pentru legume as input: return: Suntem in categoria legume
     * pentru fructe as input: return Suntem in categoria fructe
     */
    public static void main(String[] args) {
        SwitchReplacer sr = new SwitchReplacer();

        System.out.println(sr.getResponse("legume"));
        System.out.println(sr.getResponse("fructe"));
        System.out.println(sr.getResponse("unknown"));

    }
}

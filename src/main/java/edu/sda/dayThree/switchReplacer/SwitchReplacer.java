package edu.sda.dayThree.switchReplacer;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class SwitchReplacer {

    Map<String, Function<String, String>> categoryResolver = populateResolver();

    /**
     * This method receives as input the category name and computes a short text about the category. This is version 1.
     *
     * @param input String received from main
     * @return text about the category
     */
//    public String getResponse(String input) {
//        if (input.equalsIgnoreCase("legume")) {
//            return "Suntem in categoria de legume";
//        }
//
//        if (input.equalsIgnoreCase("fructe")) {
//            return "Suntem in categoria de fructe";
//        }
//
//        return "Nu am gasit categorie";
//    }

    /**
     * This method receives as input the category name and computes a short text about the category. This is version 2.
     *
     * @param input String received from main
     * @return text about the category
     */
    public String getResponse(String input) {

        //verify if input is in the map
        if (categoryResolver.get(input) != null) {
            // if input is in the map get the executable function (categoryResolver.get(input))
            // once we have the executable we use it (.apply(input))
            // --> obtain the result
            // downcast to String so that we can return the result. We know that apply returns a String since all the functions return String
            // downcast appears as redundant because we added Function<String, String> instead of Function to lines 9  55 and 56
            return (String) categoryResolver.get(input).apply(input);
        }

        return "Nu am gasit categorie";
    }

    /**
     *
     * @return a map of String, Function containing the known categories as keys and some executable functions as value. Each function has as input
     * the key and has a custom output (also String). Once a key is found the executable function can be returned and executed using .apply and the input
     */
    private Map<String, Function<String, String>> populateResolver() {
        Map<String, Function<String, String>> resolver = new HashMap<>();
        Function<String, String> funcLegume = x -> "Suntem in categoria de legume";
        Function<String, String> funcFructe = x -> "Suntem in categoria de fructe";

        resolver.put("legume", funcLegume);
        resolver.put("fructe", funcFructe);

        return resolver;
    }
}

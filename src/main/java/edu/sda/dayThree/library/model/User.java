package edu.sda.dayThree.library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Getter
    @Setter
    private String name;

    //todo check if any other fields are needed
}

package edu.sda.dayThree.library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class Library {

    @Getter
    @Setter
    private List<Book> books = new ArrayList<>();

    @Getter
    @Setter
    private List<User> users = new ArrayList<>();

    @Getter
    @Setter
    private List<Rental> rentals = new ArrayList<>();

}

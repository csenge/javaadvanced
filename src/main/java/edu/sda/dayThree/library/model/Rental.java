package edu.sda.dayThree.library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
public class Rental {

    @Getter
    @Setter
    private User userWhoIsRenting;

    @Getter
    @Setter
    private Book bookToBeRented;

//    // Add this field back PASUL 18 * - update currently existing Rental objects to have a date
//    @Getter
//    @Setter
//    private Date rentalDate;
}

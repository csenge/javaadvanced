package edu.sda.dayThree.library.helper;

import edu.sda.dayThree.library.model.Library;

public class InitializationHelper {

    public static Library initializeLibrary () {
        Library library = new Library();

        //todo populate library with data PASUL 1 - can be skipped but in that case please make sure to register at least 3 books, 3 users, 2 rentals

        return library;
    }
}

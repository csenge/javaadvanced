package edu.sda.dayThree.library;

import edu.sda.dayThree.library.helper.InitializationHelper;
import edu.sda.dayThree.library.model.Library;
import edu.sda.dayThree.library.service.SimulationService;

public class Runner {

    public static void main(String[] args) {

        Library library = InitializationHelper.initializeLibrary();

        SimulationService simulationService = new SimulationService();

        //pasul 3 - verificare
        simulationService.simulateBookRegistration(); // might need params - such as book details and pass the library as param as well

        // pasul 5 - verificare - call simultation service to print all books

        //pasul 7 - verificare - apelam simulationService simulateUserRegistration



    }
}

package edu.sda.dayThree.library.service;

import edu.sda.dayThree.library.model.Book;

public class SimulationService {
    //todo also identify params for the methods and global variables
    // NOTE global variables should be initialized as private and please try to avoid statics, ex private BookService bookService = new BookService();

    private BookService bookService = new BookService();

    public void simulateBookRegistration() {
        // add book using BookService and print result or some intermediary useful text todo
        // PASUL 3

       // bookService.addBookToLibrary(); - todo
        System.out.println("Book has been succcessfully added.");

    }

    public void simulateGetAllBooks() {
     // PASUL 5
     // todo get all books from bookservice
     // print the result
    }

    public void simulateUserRegistration() {
        // same as avobe but UserService - PASUL 7
        // call userService and print useful info about user registration
    }

    public void simulateUsercancellingMembership() {
        // PASUL 9
        // call cancel membership and print info about it - maybe print all users in library and print removed user separately
    }

    public void simulateRental() {
        // PASUL 12

    }

    public void simulateReturnal() {
        // PASUL 14

    }

    public void simulateStatisticsPrinting() {
        // PASUL 16 - call the method - it will print by default

    }

    public void simulateGetOldRentals() {

    }

    public void simulateGetOngoingRentals() {

    }
}

package edu.sda.dayThree.library.service;

import edu.sda.dayThree.library.model.Library;
import edu.sda.dayThree.library.model.User;

import java.util.List;

public class UserService {

    public void registerUser(String name, Library library) {
        // PASUL 6
        //todo register user to library - same as before, see personal training app
    }

    public void cancelMembership(String username, Library library) {
        // PASUL 8
        //todo remove user from library
        // step 1. identify user by username
        // step 2. remove found user (if found) from library. If user is not found - throw custom UserNotFoundException

    }

    public List<User> getMembers(Library library) {
        // PASUL 10
        return null; // todo get list of users - optionally: arrange them alphabetically
    }
}

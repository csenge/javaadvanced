package edu.sda.dayTwo.groceryShopping.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private float price;

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}

package edu.sda.dayTwo.groceryShopping.model;

import lombok.Getter;
import lombok.Setter;

public class Cart {

    @Getter
    @Setter
    Product[] inCartProducts = new Product[5];
}

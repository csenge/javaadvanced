package edu.sda.dayTwo.groceryShopping.model;

import lombok.Getter;
import lombok.Setter;

public class Shop {

    @Getter
    @Setter
    private Product[] products = new Product[5];

    @Getter
    @Setter
    private Cart cart;

    public Shop() {
        initShop();
        cart = new Cart();
    }

    /*
    Populate the array with some products - this array represents the menu for the user.
- Simulate the process of doing shopping:
    - ask for a product,
    - add it to the cart (array),
    - change index,
    - if index will be greater than 5 – finish shopping,
    - pay for the products.
     */


    public void initShop() {
        Product apple = new Product("Apple", 3);
        Product pear = new Product("Pear", 5);
        Product berry = new Product("Berry", 10);

        this.products[0] = apple;
        this.products[1] = pear;
        this.products[2] = berry;
    }

}

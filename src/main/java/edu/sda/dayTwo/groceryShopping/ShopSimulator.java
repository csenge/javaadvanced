package edu.sda.dayTwo.groceryShopping;

import edu.sda.dayTwo.groceryShopping.model.Shop;
import edu.sda.dayTwo.groceryShopping.service.ShopService;

import java.util.Scanner;

public class ShopSimulator {

    /*
    Simulate the process of doing shopping:
    - ask for a product,
    - add it to the cart (array),
    - change index,
    - if index will be greater than 5 – finish shopping,
    - pay for the products.
     */
    public static void main(String[] args) {
        Shop shop = new Shop();
        ShopService shopService = new ShopService();

        Scanner scanner = new Scanner(System.in);

        while (true) {
            String userInput = scanner.nextLine();
            shopService.handleUserInput(userInput, shop.getProducts(), shop.getCart().getInCartProducts());
        }
    }
}

package edu.sda.dayTwo.groceryShopping.service;

import edu.sda.dayTwo.groceryShopping.model.Product;

public class PrintService {

    public void printProductsInCart(Product[] cart) {
        System.out.println("Currently in your cart: ");
        Product[] inCartProducts = cart;
        for (int i=0; i< inCartProducts.length; i++) {
            if (inCartProducts[i] != null) {
                System.out.println(inCartProducts[i]);
            }
        }
    }
}

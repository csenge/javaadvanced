package edu.sda.dayTwo.groceryShopping.service;

import edu.sda.dayTwo.groceryShopping.model.Product;
import org.apache.commons.lang3.StringUtils;

public class ShopService {

    PrintService printService = new PrintService();

    public void handleUserInput(String userInput, Product[] products, Product[] cart) {
        // Do you have apple? Do you have grapes?
        if (StringUtils.containsIgnoreCase(userInput, "Do you have ")) {
            String response = verifyIfProductIsInStore(userInput.substring(userInput.lastIndexOf(' ') + 1).replace("?", ""), products, cart);
            System.out.println(response);
            printService.printProductsInCart(cart);
        }
    }

    private String verifyIfProductIsInStore(String product, Product[] products, Product[] cart) {
        for (int i = 0; i< products.length; i++) {
            if (products[i] != null && products[i].getName().equalsIgnoreCase(product.trim())) {
                addProductToCart(products[i], cart);
                return "Yes we have " + product;
            }
        }

        return "Unfortunately we are out of " + product;

    }


    private boolean addProductToCart(Product product, Product[] inCartProducts) {

        for (int i=0; i< inCartProducts.length; i++) {
            if (inCartProducts[i] == null) {
                inCartProducts[i] = product;
                return true;
            }
        }

        return false;
    }
}

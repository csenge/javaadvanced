package edu.sda.dayTwo.arrays;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RearrangeArrays {

    /**
     * 6. Having an array with integer numbers rearrange the elements to have the positive numbers
     * on the left and the negative numbers on the right. Do not use any sorting methods.
     */
    public static void main(String[] args) {
        List<Integer> input = Arrays.asList(2, 5, -4, -10, 34, -98, -7, 12, 43, 0, 121);
        //positive numbers -> negative numbers

        //solution 1
        Integer[] result = new Integer[input.size()];
        int positivePosition = 0;
        int negativePosition = input.size() - 1;

        for (Integer number: input) {
            if (number>= 0) {
                result[positivePosition] = number;
                positivePosition++;
            } else {
                result[negativePosition] = number;
                negativePosition--;
            }
        }

       for (int i = 0; i< result.length; i ++) {
           System.out.println(result[i]);
       }


       //solution 2 - stream
        Stream<Integer> positiveList = input.stream().filter(number -> number >= 0);
        Stream<Integer> negativeList = input.stream().filter(number -> number < 0);

        List<Integer> resultWithStream = Stream.concat(positiveList, negativeList).collect(Collectors.toList());

        System.out.println(resultWithStream);
    }
}

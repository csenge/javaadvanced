package edu.sda.dayTwo.arrays.distanceExercise;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ArrayMaxDist {


    // 1, 5, 89, 56, 45, 90
    // minDistance 1 - between 89 and 90
    //max distance 89 - between 1 and 90


    public MinMaxModel listDist(List<Integer> input) {
        MinMaxModel result = new MinMaxModel();

        //solution 1 - find min & max -> subtract-> get max distance
        // for in for - subtract numbers and find the min distance

        //get min - solution 1
        Integer min = input.get(0);

        for (Integer i: input) {
            if (i < min){
                min = i;
            }
        }

        //get min - solution 2
        Optional<Integer> minOptional = input.stream().sorted().findFirst();
        Integer minSolution2 = minOptional.get();

        //todo find max also with both solutions
        int minDistance = 9999;
        //min distance
        for (int i = 0; i< input.size() - 1; i ++) {
            for (int j = i + 1; j < input.size(); j ++) {
                if (Math.abs(input.get(i) - input.get(j)) < minDistance) {
                    minDistance = Math.abs(input.get(i) - input.get(j));
                }
            }
        }

        System.out.println("Min Distance: " + minDistance);
        return result;
    }

    public MinMaxModel arrayDists(Integer[] input) {
        MinMaxModel result = new MinMaxModel();
        //todo same as above but for array + unit tests

        return result;
    }
}

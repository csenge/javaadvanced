package edu.sda.dayTwo.arrays.distanceExercise;

import lombok.Getter;
import lombok.Setter;

public class MinMaxModel {

    @Getter
    @Setter
    Integer min;

    @Getter
    @Setter
    Integer max;
}

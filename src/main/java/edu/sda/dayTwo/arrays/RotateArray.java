package edu.sda.dayTwo.arrays;

public class RotateArray {

    /*
       7. Write a program that rotates the array elements 1 position to the right.
    */
    public Integer[] rotate(Integer[] array) {
//        Integer[] array = { 1, 7, 3, 7, 10, 1, 9 };
        // 9 1 7 3 7 10 1
        Integer[] result = new Integer[array.length];

        for (int i = 1; i <= array.length - 1; i ++) {
            result[i] = array[i-1];
        }

        result[0] = array[array.length - 1];

        for (int i = 0; i< result.length; i ++) {
            System.out.println(result[i]);
        }

        return  result;
    }
}

package edu.sda.dayTwo.shop;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    /**
     * Write a simple application that will simulate a shopping. Use only if-else flow control.
     * Consider following cases:
     * - If you would like to buy a bottle of milk – cashier will ask you for a specific amount of
     * money. You have to enter that value and verify if it is same as the cashier asked.
     * - If you would like to buy a bottle of wine – cashier will ask you if you are an adult and
     * for positive answer ask for a specific amount of money.
     */

    private static int expectedPrice = -1;
    private static String expectedAnswer = null;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, String> answerMap = new HashMap<>();
        answerMap.put("bottle of milk", "Price of milk is 5 lei");
        answerMap.put("5", "Thank you for shopping");

        while (true) {
            String userInput = scanner.nextLine();
           // handleUserInput(userInput);

            handleUserInputForceAnswer(userInput);

         //  String whatToPrint = answerMap.getOrDefault(userInput, "Invalid input");
          // handleUserInputUseMap(whatToPrint);
        }

    }

    private static void handleUserInput(String userInput) {

        if (userInput.equalsIgnoreCase("bottle of milk")) {
            System.out.println("Price of milk is: 5 lei");
            expectedPrice = 5;

        }
        //todo verify if user gave the correct amount of money for the milk
        else if (userInput.equalsIgnoreCase(String.valueOf(expectedPrice))) {
            System.out.println("Thank you for shopping");
        }

        //todo verify if user is adult
        else if (userInput.equalsIgnoreCase("bottle of wine")) {
            System.out.println(" Are you an adult?");
        }

        //todo ask for money
        else if (userInput.equalsIgnoreCase("yes")) {
            System.out.println("Price of wine is: 15 lei");
            expectedPrice = 15;
        } else {
            System.out.println("Invalid input");
        }

        //todo verify if correct amount is given - no need for this - already handled at if number 2

    }

    private static void handleUserInputForceAnswer(String userInput) {
        if (expectedAnswer == null && userInput.equalsIgnoreCase("bottle of milk")) {
            System.out.println("Price of milk is 5 lei");
            expectedAnswer = "5";
        } else if (expectedAnswer != null) {
           Integer expectedPrice = Integer.valueOf(expectedAnswer);
           Integer moneyGivenByUser = Integer.valueOf(userInput); //todo handle numberformat exception
            //todo reset expected value

           if (moneyGivenByUser < expectedPrice) {
               System.out.println("You have to give more money");
           } else {
               System.out.println("Am primit ciubuc");
           }

        } else if (expectedAnswer != null && expectedAnswer.equalsIgnoreCase(userInput)) {
            System.out.println("Thank you for shopping");
            expectedAnswer = null;
        }else {
            System.out.println("not the answer I am looking for");
        }

        }

    private static void handleUserInputUseMap(String whatToPrint) {
            System.out.println(whatToPrint);
            //set expected answer from user todo
        }
}

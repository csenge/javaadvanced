package edu.sda.dayTwo;

public class StringExercise {


    public static void main(String[] args) {
        String testString = "This is my text";

        System.out.println(testString);

        System.out.println(testString + " This text is great");

        testString.concat(" This text is great"); // will have no effect because of immutability

        System.out.println(testString);

        testString =  testString.concat(" This text is great");

        System.out.println(testString);

    }
}

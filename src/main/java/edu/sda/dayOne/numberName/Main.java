package edu.sda.dayOne.numberName;

public class Main {

    public static void main(String[] args) {
        NumberNamer numberNamer = new NumberNamer();
        System.out.println(numberNamer.getNameOfNumber(0));
        System.out.println(numberNamer.getNameOfNumber(5));
        System.out.println(numberNamer.getNameOfNumber(1));
    }
}

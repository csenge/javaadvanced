package edu.sda.dayOne.numberName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NumberNamer {

    /*
     Write an application that for any entered number between 0 and 9 will provide it’s name. For
    example for “3” program should print “three”.
     */
    public String getNameOfNumber(int number) {
        //solution 1
//        switch (number) {
//            case 0:
//                return "ZERO";
//            case 1:
//                return "ONE";
//                default:
//                    return "TWO";
//                    //todo finish switch and make sure it works properly, test for negative numbers and for numbers > 9
//        }


        //solution 2
        List<String> numberNames = Arrays.asList("ZERO", "ONE", "TWO", "THREE");
        //todo add validators

        return  numberNames.get(number);

        //solution 3, 4 -> todo implement solution with map and enum
    }
}

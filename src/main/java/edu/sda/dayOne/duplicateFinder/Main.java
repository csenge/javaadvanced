package edu.sda.dayOne.duplicateFinder;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void  main(String[] args) {
        DuplicateFinder duplicateFinder = new DuplicateFinder();
        List<String > input = Arrays.asList("abc", "asd", "thy", "abc");

        if (duplicateFinder.areDuplicates(input)) {
            System.out.println(duplicateFinder.getDuplicates(input));
        } else {
            System.out.println("No duplicates");
        }
    }
}

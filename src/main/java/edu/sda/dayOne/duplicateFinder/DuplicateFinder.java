package edu.sda.dayOne.duplicateFinder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DuplicateFinder {

    public boolean areDuplicates(List<String> input) {
        //solution 1 - stream & frequency

        //solution 2 - set
        Set<String> inputSet = new HashSet<>();
        inputSet.addAll(input);
        return input.size() != inputSet.size(); //todo
    }

    public List<String> getDuplicates(List<String> input) {
        List<String> duplicates = new ArrayList<>();
        //solution 1 - stream & frequency & add duplicates to result list


        //solution 2 - set
        Set<String> inputSet = new HashSet<>();
        inputSet.addAll(input);

        //input.removeAll(inputSet); - todo correct this

        return input; //todo
    }
}

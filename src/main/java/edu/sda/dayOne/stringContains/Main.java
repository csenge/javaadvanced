package edu.sda.dayOne.stringContains;

public class Main {

    public static void main(String[] args) {
        StringComparator comparator = new StringComparator();

        boolean result = comparator.compareStrings("The Witcher", "Witcher");
        String resultMessage = getResultMessage(result);
        System.out.println(resultMessage);

        boolean result2 = comparator.compareStrings("The Witcher", "Hunter");
        System.out.println(getResultMessage(result2));
    }

    private static String getResultMessage(boolean result) {
        return result ? "The text contains the subtext" : "Not in the text";
    }
}

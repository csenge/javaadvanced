package edu.sda.dayOne.alignExercise;

public class Main {

    public static void main(String[] args) {
        Aligner aligner = new Aligner();
        aligner.printAlignedToLeft("abc", "xyz", "sss");
        aligner.printAlignedToLeft("abcdefg", "xyz", "sssddddddaaaa");
        aligner.printAlignedToRight("abc", "xyz", "sss");
        aligner.printAlignedToRight("abcdefg", "xyz", "sssddddddaaaa");
    }
}

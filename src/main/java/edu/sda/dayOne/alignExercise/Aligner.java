package edu.sda.dayOne.alignExercise;

public class Aligner {

    /**
     *  Display any three strings of characters on one line so that they are aligned to the right
     * edge of the 15-character blocks. How to align strings to the left edge?
     */


    public void printAlignedToLeft(String text1, String text2, String text3) {
        if ((text1.length() + text2.length() + text3.length())  < 15) {
            System.out.println(text1 + text2+ text3);
        } else {
            String concatnatedText = getSecondPartOfLongString(text1, text2, text3);
            System.out.println(concatnatedText);
            //todo create a loop to handle when concatenatedText.length > 30
        }
    }

    public void printAlignedToRight(String text1, String text2, String text3) {
        if ((text1.length() + text2.length() + text3.length())  < 15) {
            String concatnatedText = text1 + text2 + text3;
            int numberOfSpaces = 15 - concatnatedText.length();
            for (int i=0; i< numberOfSpaces; i++) {
                System.out.print(" ");
            }
            System.out.println(text1 + text2+ text3);
        } else {
            String concatnatedText = getSecondPartOfLongString(text1, text2, text3);
            int numberOfSpaces = 15 - concatnatedText.length();
            for (int i=0; i< numberOfSpaces; i++) {
                System.out.print(" ");
            }
            System.out.println(concatnatedText);
            //todo create a loop to handle when concatenatedText.length > 30
        }
    }

    private String getSecondPartOfLongString(String text1, String text2, String text3) {
        String concatnatedText = text1 + text2 + text3;
        System.out.println(concatnatedText.substring(0, 15));
        concatnatedText = concatnatedText.substring(16);
        return concatnatedText;
    }


}

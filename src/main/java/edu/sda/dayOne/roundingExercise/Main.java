package edu.sda.dayOne.roundingExercise;

public class Main {

    /**
     *
     * Enter any value with several digits after the decimal point and assign it to variable
     * of type double. Display the given value rounded to two decimal places.
     */

    public static void main(String[] ags) {
        Rounder rounder = new Rounder();
        rounder.printRoundedValue(15.123456);
    }
}

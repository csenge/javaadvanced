package edu.sda.dayOne.roundingExercise;

public class Rounder {

    public void printRoundedValue(Double number) {
        System.out.printf("%.2f", number);

        String numberString = number.toString();
        int positionOfdot = numberString.indexOf('.');

        String result = numberString.substring(0, positionOfdot + 3);
        System.out.println();
        System.out.println(result);
    }
}

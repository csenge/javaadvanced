package edu.sda.dayOne.printTimes;

import java.util.Optional;

public class TextPrinter {

    public void printTextNTimes(String text, int n) {

        if (text == null) {
            System.out.println("Text is null");
            text = "Necunoscut";
        }

        if (n <= 0) {
            System.out.println("N should have positive not 0 value");
        }

        for (int i= 0; i< n; i++) {
            System.out.println(text + " iteratia " + i);
        }
    }

    public void printText(Optional<String> text) {
        if (text.isPresent()) {
            System.out.println(text.get());
        } else {
            System.out.println("Optional is empty");
        }
    }
}

package edu.sda.dayOne.printTimes;

import java.util.Optional;

public class PrintExercise {


    /**
     * 1. Use System.out.print method to print the same statement in separate lines.
     * ```
     * Hello, World!
     * Hello, World!
     */


    public static void main(String[] args) {
        TextPrinter tp = new TextPrinter();
//        tp.printTextNTimes("Hello World", 2);
//        tp.printTextNTimes("Hello World Negativ", -1);
//        tp.printTextNTimes(null, 2);

        tp.printText(Optional.of("Test"));
        tp.printText(Optional.empty());
    }
}

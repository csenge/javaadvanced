package edu.sda.dayOne.intComparator;

public class IntComparator {

    /*
    1. Write an application that will show if entered value is greater, equal or lower than 30.
     */
    public String compareTo30(int number) {
        //int --> ==
        // Integer --> equals todo

        //solution 1
//        String result = "greater";
//
//        if (number == 30) {
//            result = "equals";
//        } else if (number > 30) { -- can be removed since it is the default
//            result = "greater";
//        } else if (number < 30) {
//            result = "less";
//        }
//        System.out.println(result);
//        return result;

       //solution 2
//        if (number > 30) {
//            return "greater";
//        }
//
//        if (number < 30) {
//            return "less";
//        }
//
//        return "equals";


        return  number > 30 ? "greater" : (number < 30 ? "less" : "equals");
    }
}

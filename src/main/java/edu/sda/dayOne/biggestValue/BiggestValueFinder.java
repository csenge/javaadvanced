package edu.sda.dayOne.biggestValue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BiggestValueFinder {

    /*Write an application that will find biggest value within array of int variables.
    - check your application using randomly generated array (use Random class),
    - check your application at least 5 times in a loop (generate random array -> print
array to the console -> find biggest value -> print biggest value -> manually verify
results).
     */

    //todo implement the same thing with array - optional
    // todo - optional: find reverse method
    public int getBiggestValue(List<Integer> list) {
        ValuePrinter valuePrinter = new ValuePrinter();
        Integer biggestValue = -1000;
        // validare list.size() > 0 ---> !list.isEmpty()
        Optional<Integer>  biggestOptional =
                list.stream()
                    .sorted(Collections.reverseOrder())
                    .findFirst();

       if (biggestOptional.isPresent()) {
           biggestValue = biggestOptional.get();
       }

       valuePrinter.printValue(biggestValue);

       return biggestValue;
    }
}

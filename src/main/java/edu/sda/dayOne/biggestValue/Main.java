package edu.sda.dayOne.biggestValue;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        BiggestValueFinder finder = new BiggestValueFinder();

        System.out.println(finder.getBiggestValue(Arrays.asList(2,5,-10, 89, 43, 65, -7, 0, 5, 123, 54, 83)));
        System.out.println(finder.getBiggestValue(Arrays.asList()));

    }
}
